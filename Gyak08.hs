import Data.List

sum' :: [Int] -> Int
sum' [] = 0
sum' (x:y) = x + sum' y

  -- n  :: Int
  -- ns :: [Int]

range :: Int -> Int -> [Int]
range a b | a == b = [a]

last' :: [Int] -> Int
last' [n] = n -- pontosan 1 elemű a lista
last' (_:ns) = last' ns

-- vagy
-- last' (n : []) = n
{-
and' :: [Bool] -> Bool
and' [] = True
and' (b : bs)
  | b = and' bs
  | otherwise = False
-}

and' :: [Bool] -> Bool
and' [] = True
and' (n:[]) = n
and' (n:ns) = n && and' ns
{-
or' :: [Bool] -> Bool
or' [] = False
or' (b:bs)
  | b = True
  | otherwise = or' bs
-}

or' :: [Bool] -> Bool
or' [] = False
or' (b:bs) = b || or' bs

allPositive :: [Int] -> Bool
allPositive list = and [ n > 0 | n <- list ]

repeat' :: Char -> [Char]
repeat' c = c : repeat' c

{-
replicate' :: Int -> Char -> [Char]
replicate' n c = take n (repeat' c)
-}

replicate' :: Int -> Char -> [Char]
replicate' n c
  | n <= 0    = []
  | otherwise = c : replicate' (n - 1) c
  --            ^^^^^^^^^^^^^^^^^^^^^^^^
  --              n hosszú lista

breakOn :: Char -> [Char] -> ([Char], [Char])
breakOn c (first:remaining)
  | c == first = ("", first:remaining)
  | otherwise = (first : before, after)
    where
      before :: [Char]
      after  :: [Char]
      (before, after) = breakOn c remaining
breakOn c [] = ([], [])
