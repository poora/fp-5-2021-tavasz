-- Gyak02.hs

{- több soros
   komment -}

-- Példák konstansokra:
n :: Int  -- (n típusa Int)
n = 42

-- Nem szabad felülírni konstansokat
-- n = 10

s :: String
s = "Haskell"

-- Példák függvényekre:
even' :: Int -> Bool
even' n = mod n 2 == 0

odd' :: Int -> Bool
odd' n = not (even' n)

max3 :: Integer -> Integer -> Integer -> Integer
max3 a b c = max (max a b) c

-- odd' n = n `mod` 2 == 1
-- odd' n = n `mod` 2 /= 0

-- divides a b =

-- Nem teljes megoldás:
pythagorean a b c = a2 + b2 == c2
  where
    a2 = a ^ 2
    b2 = b ^ 2
    c2 = c ^ 2

-- int n = 0
