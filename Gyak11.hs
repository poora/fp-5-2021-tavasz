data Day = Mon | Tue | Wed | Thu | Fri | Sat | Sun
--         ^^^   ^^^   ^^^   ^^^   ^^^   ^^^   ^^^
--                     adatkonstruktorok
  deriving (Show)

isWeekend :: Day -> Bool
isWeekend Sat = True
isWeekend Sun = True
isWeekend _   = False

-- totális függvény, minden lehetőség le lett kezelve
nextDay :: Day -> Day
nextDay Mon = Tue
nextDay Tue = Wed
nextDay Wed = Thu
nextDay Thu = Fri
nextDay Fri = Sat
nextDay Sat = Sun
nextDay Sun = Mon

-- T :: Int -> Int -> Time
data Time = T Int Int
  deriving (Eq) -- Show: lehessen képernyőre írni
                      -- Eq: lehessen használni (==)-t és (/=)-t

-- vagy kézzel tesszük a Show osztályba a Time típust:
instance Show Time where
  show (T h m)
    | m < 10    = show h ++ ":0" ++ show m
    | otherwise = show h ++ ":" ++ show m

eqTime :: Time -> Time -> Bool
eqTime t1 t2 = t1 == t2
  
{-
showTime :: Time -> String
showTime (T h m)
  | m < 10    = show h ++ ":0" ++ show m
  | otherwise = show h ++ ":" ++ show m
-}

-- 8. feladat isBetween
-- isEarlier használható!

-- 9. feladat time
time :: Int -> Int -> Time
time h m
  | h < 0 || h > 23 = error ("time: invalid hour: " ++ show h)
  | m < 0 || m > 59 = error ("time: invalid minute: " ++ show m)
  | otherwise = T h m

data Distance = Meters Integer | Kilometers Integer
  deriving Eq

{-
data Distance = Meters Integer | Kilometers Integer
-- nincs deriving!

-- ezt írná meg a fordítóprogram helyettünk,
-- ha használjuk a deriving Eq-t:
instance Eq Distance where
  Meters m1      == Meters m2      = m1 == m2
  Kilometers km1 == Kilometers km2 = km1 == km2
  _              == _              = False

  distance1      /= distance2      = not (distance1 == distance2)

-}

{-
data Distance = Meters Integer | Kilometers Integer
-- nincs deriving!

instance Eq Distance where
  Meters m1      == Meters m2      = m1 == m2
  Kilometers km1 == Kilometers km2 = km1 == km2
  Meters m       == Kilometers km  = m == km * 1000
  Kilometers km  == Meters m       = km * 1000 == m

  distance1      /= distance2      = not (distance1 == distance2)
-}

{-
instance Show Distance where
  show (Meters m)      = show m ++ " m"
  show (Kilometers km) = show km ++ " km"
-}
