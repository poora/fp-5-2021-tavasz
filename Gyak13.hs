import Data.Char -- toUpper, toLower

data PersonalInfo = Name String
                  | NameEmail String String
                  | NameBirthYear String Int
                  | NameEmailBirthYear String String Int
  deriving Show

getName :: PersonalInfo -> String
getName (Name name) = name
getName (NameEmail name _) = name
getName (NameBirthYear name _) = name
getName (NameEmailBirthYear name _ _) = name

-- példa egymásba ágyazott adatszerkezetekre:

data Distro = Debian | Ubuntu | Fedora | Arch
  deriving (Show)

data OS = Windows Int | Linux Distro String | Mac String
  deriving (Show)

isWindows10 :: OS -> Bool
isWindows10 (Windows 10) = True
isWindows10 _ = False

isUbuntu2004 :: OS -> Bool
isUbuntu2004 (Linux Ubuntu "20.04") = True
isUbuntu2004 _ = False

--
{-
data Maybe a = Nothing | Just a

  // Maybe Int = Nothing | Just Int   // a = Int
  // Maybe Char = Nothing | Just Char // a = Char
-}

-- Maybe gyártása:
safeDiv :: Int -> Int -> Maybe Int
safeDiv a 0 = Nothing
safeDiv a b = Just (a `div` b)

data Time = T Int Int
  deriving (Eq, Show)

time :: Int -> Int -> Maybe Time
time h m
  | h < 0 || h > 23 = Nothing
  | m < 0 || m > 59 = Nothing
  | otherwise = Just (T h m)

-- Maybe feldolgozása:
showMaybe :: Show a => Maybe a -> String
showMaybe Nothing  = "Itt nincs semmi"
showMaybe (Just x) = "Az eredmeny: " ++ show x

modify :: (a -> Maybe a) -> [a] -> [a]
modify f [] = []
modify f (x:xs) = case f x of
                    Nothing -> xs
                    Just first -> first : xs
{-
modify :: (a -> Maybe a) -> [a] -> [a]
modify f [] = []
modify f (x:xs) = helper (f x)
  where
    helper Nothing = xs
    helper (Just first) = first : xs
-}
conditionalMin :: (Int -> Bool) -> [Int] -> Int
conditionalMin pred list = case filter pred list of
                             [] -> -1
                             [n] -> n
                             newList -> minimum newList

-- Akasztófajáték

type ABC = [Char]

abc :: ABC
abc = ['A'..'Z']

-- típusszinonimák
type Riddle       = String
type RightGuesses = [Char]
type WrongGuesses = [Char]

--           St String [Char] [Char]
data State = St Riddle RightGuesses WrongGuesses
  deriving (Eq, Show)

-- isValidLetter :: Char -> ABC -> Bool

{-
--            [Char] -> String -> Maybe State
startState ::    ABC -> Riddle -> Maybe State
startState abc s
  | all (\c -> c == ' ' || isValidLetter c) s =
  | otherwise =   
-}
