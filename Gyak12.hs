-- https://people.inf.elte.hu/poor_a/fp10.pdf

takeWhile' :: (a -> Bool) -> [a] -> [a]
takeWhile' pred [] = []
takeWhile' pred (x:xs)
  | pred x    = x : takeWhile' pred xs
  | otherwise = []

dropWhile' :: (a -> Bool) -> [a] -> [a]
dropWhile' pred [] = []
dropWhile' pred list@(x:xs)
  | pred x    = dropWhile' pred xs
  | otherwise = list -- vagy x:xs

-- https://people.inf.elte.hu/poor_a/fp11.pdf

-- 1. dropSpaces
-- dropWhile itt is jól jön, hasonlít a
-- dropWordhöz

-- dropWhile' :: (a -> Bool) -> [a] -> [a] // a = Char
-- dropWhile' :: (Char -> Bool) -> [Char] -> [Char]


dropSpaces :: String -> String
--            [Char] -> [Char]
dropSpaces s = dropWhile (\c -> c == ' ') s

-- 2. trim
-- dropSpaces + reverse hasznos
-- reverse "haskell   " == "   lleksah"

trim :: String -> String
trim s = reverse (dropSpaces (reverse (dropSpaces s)))

zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' f [] _  = []
zipWith' f _  [] = []
zipWith' f (x:xs) (y:ys) = f x y : zipWith' f xs ys

-- hf: zip definiálni zipWith függvénnyel

{-
zipWith' f (x:xs) (y:ys) = f x y : zipWith' f xs ys
zipWith' f _      _      = []
-}


elem' :: Eq a => a -> [a] -> Bool
elem' e []     = False
elem' e (x:xs) = e == x || elem' e xs

addGreater :: (Ord a, Num a) => a -> [a] -> a
addGreater x [] = 0
addGreater x (y:ys)
  | y > x = y + addGreater x ys
  | otherwise = addGreater x ys
