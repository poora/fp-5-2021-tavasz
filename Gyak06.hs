{-
powersOfTwo :: [Integer]
powersOfTwo = [ | ... ]
-}

allPositive :: [Int] -> Bool
allPositive list = null [ n | n <- list, n <= 0 ]

allPositive' :: [Int] -> Bool
allPositive' list = and [ n > 0 | n <- list ]

courses :: [(String, [(String, String, String)])]
courses = [ ("Programozasi nyelvek II.", [("Horvath", "Istvan", "BDE91E"), ("Fodros", "Aniko", "DDA3KX")]),
            ("Imperativ programozas",    [("Nemeth", "Eniko", "ALX1K0"), ("Horvath", "Istvan", "BDE91E")]),
            ("Funkcionalis programozas", [("Kiss", "Elemer", "ABCDE6"), ("Nagy", "Jakab", "CDE560")])]

students = [ neptun | (course, ppl) <- courses, course == "Funkcionalis programozas", (_, _, neptun) <- ppl]

naturals :: [(Integer, Integer)]
naturals = [ (s - a, a) | s <- [0..], a <- [0..s] ]
