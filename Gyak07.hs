fact :: Int -> Int
fact 0 = 1
fact n = n * fact (n - 1)

fact' :: Integer -> Integer
fact' 0 = 1
fact' n
  | n > 0 = n * fact' (n - 1)

fib :: Integer -> Integer
fib 0 = 0
fib 1 = 1
fib n = fib (n - 1) + fib (n - 2)

fib' :: Integer -> Integer
fib' 0 = 0
fib' 1 = 1
fib' n
  | n > 0 = fib' (n - 1) + fib' (n - 2)

memoized_fib :: Int -> Integer
memoized_fib = (map fib [0 ..] !!)
   where fib 0 = 0
         fib 1 = 1
         fib n = memoized_fib (n-2) + memoized_fib (n-1)





pow :: Int -> Int -> Int
pow a 0 = 1
pow a b = a * pow a (b - 1)

range :: Int -> Int -> [Int]
-- range a a = [a]  -- NEM JÓ!
range a b
  | a == b = [a]
  | a < b = a : range (a + 1) b
  | otherwise = a : range (a - 1) b

-- otherwise :: Bool
-- otherwise = True

length' :: [Int] -> Int
length' [] = 0
length' (n:ns) = 1 + length' ns

  -- n  :: Int
  -- ns :: [Int]

minimum' :: [Integer] -> Integer
minimum' (first:rest)
  | null rest = first
  | first <= m = first
  | first > m = m

  where
    m = minimum' rest

everySecond :: String -> String
everySecond "" = ""
everySecond [x] = ""
everySecond (x:y:xs) = y : everySecond xs

elem' :: Char -> [Char] -> Bool
elem' _ [] = False
elem' a (x : xs)
 | a == x = True
 | otherwise = elem' a xs


value :: Int -> [(Int, String)] -> String
value _ [] = error "Hoppa hiba"
value k ( (kx, vx) : xs) 
    | k == kx = vx
    | otherwise = value k xs
