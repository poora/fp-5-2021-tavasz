head' :: [int] -> int
head' (first : _ ) = first

take' :: Int -> [a] -> [a]
take' n _
  | n <= 0     = []
take' _ []     = []
take' n (x:xs) = x : take' (n - 1) xs

drop' :: Int -> [a] -> [a] -- hasonló a take-hez
drop' n list
  | n <= 0     = list
drop' _ []     = []
drop' n (_:xs) = drop (n - 1) xs

-- 4. langAndRegion
-- nem polimorf, nem rekurzív
-- take + drop

langAndRegion :: String -> (String, String)
langAndRegion s = (take 2 s , drop 3 s)


zip' :: [a] -> [b] -> [(a,b)]
zip' [] _ = []
zip' _ [] = []
zip' (x:xs) (y:ys) = (x,y) : zip' xs ys


unzip' :: [(a,b)] -> ([a], [b])
unzip' [] = ([], [])
unzip' (p:ps) = (fst p : fst rs, snd p : snd rs)
--                           ^^              ^^
--                           ([a], [b])      ([a], [b])
--                       ^^^^^^          ^^^^^^^
--                           [a]             [b]

  where
    rs = unzip' ps

{-
unzip' :: [(a,b)] -> ([a], [b])
unzip' []     = 
unzip' (p:ps) = 

unzip' [('a',1),('b',2),('c',3)] == (['a','b','c'], [1,2,3])
        ^^^^^^^ ^^^^^^^^^^^^^^^
           p          ps

rekurzív függvényhívás:
unzip' [('b', 2), ('c', 3)] == (['b','c'], [2,3])
fst (unzip' [('b', 2), ('c', 3)]) == ['b','c']
snd (unzip' [('b', 2), ('c', 3)]) == [2,3]
-}




{-
"elso\nmasodik\n\nnegyedik\n"

--------------
elso
masodik

negyedik
--------------
-}


empty :: String -> [Int]
empty s = helper (zip [1..] (lines s))
  where
    helper :: [(Int, String)] -> [Int]
    helper ((n, line) : rest)
      | null line = n : helper rest
      | otherwise = helper rest
    helper [] = []
