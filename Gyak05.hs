import Data.Char

headInt :: [Int] -> Int
headInt (first : rest) = first

nullInt :: [Int] -> Bool
nullInt [] = True
nullInt _  = False

isSingletonInt :: [Int] -> Bool
isSingletonInt []           = False
isSingletonInt (first : []) = True
isSingletonInt _            = False

isSingletonInt' :: [Int] -> Bool
isSingletonInt' (first : []) = True  -- 1 elemű lista
isSingletonInt' _            = False -- 0, 2, 3, 4, ...
                                     -- elemű lista

firstTwo :: [Int] -> (Int, Int)
firstTwo (first : second : rest) = (first, second)

-- toUpperFirst :: String -> String
-- toUpperFirst :: [Char] -> [Char]
